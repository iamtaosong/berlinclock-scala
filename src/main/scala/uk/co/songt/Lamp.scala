package uk.co.songt

object Lamps {

  sealed abstract class Lamp(representation: String) {
    override def toString: String = representation
  }

  case object RedLamp extends Lamp("R")

  case object YellowLamp extends Lamp("Y")

  case object OffLamp extends Lamp("O")
}
