package uk.co.songt

import java.text.SimpleDateFormat
import java.util.Calendar

import uk.co.songt.Rows._


object BerlinClock {

  def setTime(hours: Int, minutes: Int, seconds: Int): GraphicBerlinClock = {
    val secondRow =  SecRow.lampArray(seconds)
    val topHours =  TopHourRow.lampArray(hours)
    val bottomHours =  BottomHourRow.lampArray(hours)
    val topMins =  TopMinRow.lampArray(minutes)
    val bottomMins =  BottomMinRow.lampArray(minutes)
    GraphicBerlinClock(secondRow, topHours, bottomHours, topMins, bottomMins)
  }

  def parseInput(args: Array[String]): (Int, Int, Int) = {
    try {
      val split = args(0).split(":").map(_.toInt)
      (split(0), split(1), split(2))
    } catch {
      case t: Throwable =>
        throw new IllegalArgumentException
    }
  }

  def main(args: Array[String]): Unit = {
    try {
      val (hours, minutes, seconds) = parseInput(args)
      val bc = BerlinClock.setTime(hours, minutes, seconds)
      println(bc)
    } catch {
      case x: IllegalArgumentException =>
        System.exit(-1)
    }
  }
}

