package uk.co.songt

import uk.co.songt.Lamps.{Lamp, OffLamp}

import scala.annotation.tailrec

trait RowLamp {
  val Five = 5
  val Two = 2

  def getDevidedByFive(dividend: Int): Int = dividend / Five

  def getModularFive(dividend: Int): Int = dividend % Five

  def getModularTwo(dividend: Int): Int = dividend % Two

  def lampArray(timeValue : Int ):Array[Lamp]

  def oneColorLights(total: Int, lit: Int, litLamp: Lamp): Array[Lamp] = {

    @tailrec
    def go(acc: Array[Lamp]): Array[Lamp] = {
      acc.size match {
        case x if x == total => acc
        case x if x < lit => go(acc :+ litLamp)
        case x => go(acc :+ OffLamp)
      }
    }
    go(Array[Lamp]())
  }

  def twoColorLights(total: Int, lit: Int, offset: Int, litLamp1: Lamp, litLamp2: Lamp): Array[Lamp] = {

    @tailrec
    def go(acc: Array[Lamp]): Array[Lamp] = {
      acc.size match {
        case x if x == total => acc
        case x if x < lit && (x + 1) % offset == 0 => go(acc :+ litLamp2)
        case x if x < lit => go(acc :+ litLamp1)
        case x => go(acc :+ OffLamp)
      }
    }
    go(Array[Lamp]())
  }
}
