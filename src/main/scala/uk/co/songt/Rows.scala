package uk.co.songt

import uk.co.songt.Lamps.{Lamp, RedLamp, YellowLamp}
import uk.co.songt.Rows.TopHourRow._


object Rows {

  sealed abstract  class Row( val lamps: Int ){
    val totalLamps = lamps
  }
  object SecRow extends Row(1) with RowLamp{

    def lampArray(timeValue : Int ):Array[Lamp] = oneColorLights(totalLamps, 1-getModularTwo(timeValue) , YellowLamp)
  }

  object TopHourRow extends Row(4) with RowLamp{

    def lampArray(timeValue : Int ):Array[Lamp] = oneColorLights(totalLamps, getDevidedByFive(timeValue) , RedLamp)
  }

  object BottomHourRow extends Row(4)with RowLamp{
    def lampArray(timeValue : Int ):Array[Lamp] = oneColorLights(totalLamps, getModularFive(timeValue) , RedLamp)
  }

  object TopMinRow extends Row(11) with RowLamp{

    def lampArray(timeValue : Int ):Array[Lamp] = twoColorLights(totalLamps, getDevidedByFive(timeValue), 3, YellowLamp, RedLamp)
  }

  object BottomMinRow extends Row(4) with RowLamp{
    def lampArray(timeValue : Int ):Array[Lamp] = oneColorLights(totalLamps, getModularFive(timeValue) , YellowLamp)
  }

}
