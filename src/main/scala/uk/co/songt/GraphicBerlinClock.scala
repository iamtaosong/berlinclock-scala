package uk.co.songt

import uk.co.songt.Lamps.{Lamp, OffLamp}


case class GraphicBerlinClock(sec: Array[Lamp] = Array.fill(1)(OffLamp),
                              topHours: Array[Lamp] = Array.fill(4)(OffLamp),
                              bottomHours: Array[Lamp] = Array.fill(4)(OffLamp),
                              topMins: Array[Lamp] = Array.fill(11)(OffLamp),
                              bottomMins: Array[Lamp] = Array.fill(4)(OffLamp)) {
  override def toString: String = {
    def print(arr: Array[Lamp]) = arr.mkString("")
    f"${print(sec)} ${print(topHours)} ${print(bottomHours)} ${print(topMins)} ${print(bottomMins)}"
  }
}


