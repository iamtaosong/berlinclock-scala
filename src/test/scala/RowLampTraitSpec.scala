import org.scalatest.{FlatSpecLike, Matchers}
import uk.co.songt.Lamps.{Lamp, OffLamp, RedLamp, YellowLamp}
import uk.co.songt.RowLamp

class RowLampTraitSpec  extends FlatSpecLike with Matchers with RowLamp{

  def lampArray(timeValue : Int ):Array[Lamp]={
    (Array(OffLamp))
  }

  "getDevidedByFive with input 5" should " get 1 " in{
    assertResult(1)(getDevidedByFive(5))
  }

  "getDevidedByFive with input 6" should " get 1 " in{
    assertResult(1)(getDevidedByFive(5))
  }

  "getDevidedByFive with input 10" should " get 2 " in{
    assertResult(2)(getDevidedByFive(10))
  }

  "getModularFive with input 10" should " get 0 " in{
    assertResult(0)(getModularFive(10))
  }

  "getModularFive with input 11" should " get 1 " in{
    assertResult(1)(getModularFive(11))
  }
  "getModularTwo with input 1" should " get 1 " in{
    assertResult(1)(getModularTwo(1))
  }

  "getModularFive with input 2" should " get 0" in{
    assertResult(0)(getModularTwo(2))
  }

  "oneColorLights with 4 redlapm ,and 2 On " should " get R R O O" in{
    assertResult(Array(RedLamp, RedLamp, OffLamp,OffLamp))( oneColorLights(4,2,RedLamp))
  }

  "oneColorLights with 0 redlapm ,and 2 On " should " get empty array " in{
    assertResult(Array())( oneColorLights(0,2,RedLamp))
  }

  "oneColorLights with 1 redlapm ,and 2 On " should " get R" in{
    assertResult(Array(RedLamp))( oneColorLights(1,2,RedLamp))
  }

  "twoColorLights with 5 lights " should " get YYROO" in{
    assertResult(Array(YellowLamp,YellowLamp,RedLamp,OffLamp,OffLamp))( twoColorLights(5, 3, 3, YellowLamp, RedLamp) )
  }
}
