import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpecLike, Matchers}
import uk.co.songt.BerlinClock
import uk.co.songt.Lamps.{OffLamp, YellowLamp}
import uk.co.songt.Rows.SecRow

class BerlinClockSpec extends FlatSpecLike with Matchers with MockitoSugar {

  "00:00:00" should "get Y OOOO OOOO OOOOOOOOOOO OOOO" in {
    BerlinClock.setTime(0, 0, 0).toString should be ("Y OOOO OOOO OOOOOOOOOOO OOOO")
  }

  "13:17:01" should "get O RROO RRRO YYROOOOOOOO YYOO" in {
    BerlinClock.setTime(13, 17, 1).toString should be ("O RROO RRRO YYROOOOOOOO YYOO")
  }

  "23:59:59" should "get O RRRR RRRO YYRYYRYYRYY YYYY" in {
    BerlinClock.setTime(23, 59, 59).toString should be ("O RRRR RRRO YYRYYRYYRYY YYYY")
  }

  " 24:00:00" should "get Y RRRR RRRR OOOOOOOOOOO OOOO" in {
    BerlinClock.setTime(24, 0, 0).toString should be ("Y RRRR RRRR OOOOOOOOOOO OOOO")
  }

}
