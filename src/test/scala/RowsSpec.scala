import org.scalatest.mock.MockitoSugar
import org.scalatest.{FlatSpecLike, Matchers}
import uk.co.songt.Lamps.{RedLamp, OffLamp, YellowLamp}
import uk.co.songt.Rows._

class RowsSpec extends FlatSpecLike with Matchers with MockitoSugar {

  "Time with 1 seconds" should " get O " in {
    SecRow.lampArray(1) should equal (Array(OffLamp))
  }

  "Time with 0 seconds" should " get Y " in {

    SecRow.lampArray(0) should equal (Array(YellowLamp))
  }

  "Time with 2 seconds" should " get Y " in {
    SecRow.lampArray(2) should equal (Array(YellowLamp))
  }

  "Time with 18 minutues" should "YYROOOOOOOO YYYO " in {
    TopMinRow.lampArray(18) should equal (Array(YellowLamp,YellowLamp,RedLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp))
    BottomMinRow.lampArray(18) should equal (Array(YellowLamp,YellowLamp,YellowLamp,OffLamp))
  }

  "Time with 10 minutues" should "YYOOOOOOOOO OOOO " in {
    TopMinRow.lampArray(10) should equal (Array(YellowLamp,YellowLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp))
    BottomMinRow.lampArray(10) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp))
  }

  "Time with 0 minutues" should "OOOOOOOOOOO OOOO " in {
    TopMinRow.lampArray(0) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp,OffLamp))
    BottomMinRow.lampArray(0) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp))
  }


  "Time with 13 hours" should "RROOOOOOOOO RRRO " in {
    TopHourRow.lampArray(13) should equal (Array(RedLamp,RedLamp,OffLamp,OffLamp ))
    BottomHourRow.lampArray(13) should equal (Array(RedLamp,RedLamp,RedLamp,OffLamp))
  }

  "Time with 10 hours" should "YYOOOOOOOOO OOOO " in {
    TopHourRow.lampArray(10) should equal (Array(RedLamp,RedLamp,OffLamp,OffLamp ))
    BottomHourRow.lampArray(10) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp))
  }

  "Time with  0 hours" should "OOOOOOOOOOO OOOO " in {
    TopHourRow.lampArray(0) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp ))
    BottomHourRow.lampArray(0) should equal (Array(OffLamp,OffLamp,OffLamp,OffLamp))
  }
}
